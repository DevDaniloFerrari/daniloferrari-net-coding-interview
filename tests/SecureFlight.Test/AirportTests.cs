using Moq;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using Xunit;

namespace SecureFlight.Test
{
    public class AirportTests
    {
        [Fact]
        public void Update_Succeeds()
        {
            Mock<IRepository<Airport>> mockRepository = new Mock<IRepository<Airport>>();

            mockRepository.Object.Update(new Airport());

            mockRepository.Verify(x => x.SaveChanges(), Times.Once);
        }
    }
}
